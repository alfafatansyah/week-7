/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32g0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#define n_0 0x3f // 0b00111111
#define n_1 0x06 // 0x00000110
#define n_2 0x5B // 0b01011011
#define n_3 0x4F // 0b01001111
#define n_4 0x66 // 0b01100110
#define n_5 0x6D // 0b01101101
#define n_6 0x7D // 0b01111101
#define n_7 0x07 // 0b00000111
#define n_8 0x7F // 0b01111111
#define n_9 0x6F // 0b01101111

#define a_on HAL_GPIO_WritePin(a_GPIO_Port, a_Pin, 1);
#define b_on HAL_GPIO_WritePin(b_GPIO_Port, b_Pin, 1);
#define c_on HAL_GPIO_WritePin(c_GPIO_Port, c_Pin, 1);
#define d_on HAL_GPIO_WritePin(d_GPIO_Port, d_Pin, 1);
#define e_on HAL_GPIO_WritePin(e_GPIO_Port, e_Pin, 1);
#define f_on HAL_GPIO_WritePin(f_GPIO_Port, f_Pin, 1);
#define g_on HAL_GPIO_WritePin(g_GPIO_Port, g_Pin, 1);
#define h_on HAL_GPIO_WritePin(h_GPIO_Port, h_Pin, 1);

#define a_toggle HAL_GPIO_TogglePin(a_GPIO_Port, a_Pin);
#define b_toggle HAL_GPIO_TogglePin(b_GPIO_Port, b_Pin);
#define c_toggle HAL_GPIO_TogglePin(c_GPIO_Port, c_Pin);
#define d_toggle HAL_GPIO_TogglePin(d_GPIO_Port, d_Pin);
#define e_toggle HAL_GPIO_TogglePin(e_GPIO_Port, e_Pin);
#define f_toggle HAL_GPIO_TogglePin(f_GPIO_Port, f_Pin);
#define g_toggle HAL_GPIO_TogglePin(g_GPIO_Port, g_Pin);
#define h_toggle HAL_GPIO_TogglePin(h_GPIO_Port, h_Pin);

#define s0_on HAL_GPIO_WritePin(s0_GPIO_Port, s0_Pin, 1);
#define s0_off HAL_GPIO_WritePin(s0_GPIO_Port, s0_Pin, 0);
#define s1_on HAL_GPIO_WritePin(s1_GPIO_Port, s1_Pin, 1);
#define s1_off HAL_GPIO_WritePin(s1_GPIO_Port, s1_Pin, 0);
#define s2_on HAL_GPIO_WritePin(s2_GPIO_Port, s2_Pin, 1);
#define s2_off HAL_GPIO_WritePin(s2_GPIO_Port, s2_Pin, 0);
#define s3_on HAL_GPIO_WritePin(s3_GPIO_Port, s3_Pin, 1);
#define s3_off HAL_GPIO_WritePin(s3_GPIO_Port, s3_Pin, 0);

#define s0_toggle HAL_GPIO_TogglePin(s0_GPIO_Port, s0_Pin);
#define s1_toggle HAL_GPIO_TogglePin(s1_GPIO_Port, s1_Pin);
#define s2_toggle HAL_GPIO_TogglePin(s2_GPIO_Port, s2_Pin);
#define s3_toggle HAL_GPIO_TogglePin(s3_GPIO_Port, s3_Pin);

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define c_Pin GPIO_PIN_9
#define c_GPIO_Port GPIOB
#define T_NRST_Pin GPIO_PIN_2
#define T_NRST_GPIO_Port GPIOF
#define T_NRST_EXTI_IRQn EXTI2_3_IRQn
#define s0_Pin GPIO_PIN_0
#define s0_GPIO_Port GPIOA
#define s1_Pin GPIO_PIN_1
#define s1_GPIO_Port GPIOA
#define T_VCP_TX_Pin GPIO_PIN_2
#define T_VCP_TX_GPIO_Port GPIOA
#define T_VCP_RX_Pin GPIO_PIN_3
#define T_VCP_RX_GPIO_Port GPIOA
#define s2_Pin GPIO_PIN_4
#define s2_GPIO_Port GPIOA
#define s3_Pin GPIO_PIN_5
#define s3_GPIO_Port GPIOA
#define g_Pin GPIO_PIN_0
#define g_GPIO_Port GPIOB
#define d_Pin GPIO_PIN_1
#define d_GPIO_Port GPIOB
#define f_Pin GPIO_PIN_2
#define f_GPIO_Port GPIOB
#define h_Pin GPIO_PIN_9
#define h_GPIO_Port GPIOA
#define LD3_Pin GPIO_PIN_6
#define LD3_GPIO_Port GPIOC
#define T_JTMS_Pin GPIO_PIN_13
#define T_JTMS_GPIO_Port GPIOA
#define T_JTCK_Pin GPIO_PIN_14
#define T_JTCK_GPIO_Port GPIOA
#define a_Pin GPIO_PIN_4
#define a_GPIO_Port GPIOB
#define b_Pin GPIO_PIN_5
#define b_GPIO_Port GPIOB
#define e_Pin GPIO_PIN_8
#define e_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
