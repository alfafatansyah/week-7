/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
RTC_TimeTypeDef gtime;
RTC_DateTypeDef gdate;

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
RTC_HandleTypeDef hrtc;

TIM_HandleTypeDef htim16;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
int segment[7] = {a_Pin, b_Pin, c_Pin, d_Pin, e_Pin, f_Pin, g_Pin};
short s, s0, s1, s2, s3;
int s2_status;

short count_numb, count_second, count_minute, count_hour;

uint32_t time_now, time_last, time_button, time_press, time_release;

char jam, menit, detik, mildetik;

char uart_buf[50];
int uart_buf_len;

_Bool button_last, button_now, button_is_press, button_long_press, mode, stop_watch, button_main, button_stopwatch, stopwatch_reset, pause;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_TIM16_Init(void);
static void MX_RTC_Init(void);
/* USER CODE BEGIN PFP */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{	
	
}

/*void HAL_GPIO_EXTI_Rising_Callback(uint16_t GPIO_Pin)
{
	if(GPIO_Pin == GPIO_PIN_15) // If The INT Source Is EXTI Line9 (A9 Pin)
  {
		HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_6); // Toggle The Output (LED) Pin
  }
}*/

void test_led(void);
int convert_to(int x);
void print_to(int x);
void selector(int x);
void set_time (void);
void get_time(void);
void display_sevensegment(short a, short b, short c, short d);
void watch_mode(void);
void counter_mode(void);
void switch_mode (int sw);


/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  MX_TIM16_Init();
  MX_RTC_Init();
  /* USER CODE BEGIN 2 */
	HAL_TIM_Base_Start_IT(&htim16);
	//set_time();
	
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
		button_now = HAL_GPIO_ReadPin(btn_GPIO_Port, btn_Pin);
		
		if(button_last == 1 && button_now == 0)	// button pressed
		{
			time_press = HAL_GetTick();
			button_is_press = 1;
			button_long_press = 0;
		}
		
		else if(button_last == 0 && button_now == 1) // button released
		{
			time_release = HAL_GetTick();
			button_is_press = 0;		
			
			time_button = time_release - time_press;
			
			if(time_button < 3000 && mode == 1)
			{
				HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_6);
				stop_watch = !stop_watch;
				stopwatch_reset = 1;
			}
		}

		if(button_is_press == 1 && button_long_press == 0)
		{
			time_button = HAL_GetTick() - time_press;
			
			if(time_button > 3000)
			{
				if(stopwatch_reset == 0)
				{
					mode = !mode;
					button_long_press = 1;
					button_stopwatch = 1;
					button_main = 1;
				}
				else if(stopwatch_reset == 1 && stop_watch == 0)
				{
					count_numb = 0;
					count_second = 0;
					count_minute = 0;
					count_hour = 0;
					stopwatch_reset = 0;
					button_is_press = 0;
				}
			}
		}	

		switch_mode(mode);

		button_last = button_now;
		
		//watch_mode();
								
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Configure the main internal regulator output voltage
  */
  HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Configure LSE Drive Capability
  */
  HAL_PWR_EnableBkUpAccess();
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSIDiv = RCC_HSI_DIV1;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV1;
  RCC_OscInitStruct.PLL.PLLN = 8;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the peripherals clocks
  */
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;

  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief RTC Initialization Function
  * @param None
  * @retval None
  */
static void MX_RTC_Init(void)
{

  /* USER CODE BEGIN RTC_Init 0 */

  /* USER CODE END RTC_Init 0 */

  /* USER CODE BEGIN RTC_Init 1 */

  /* USER CODE END RTC_Init 1 */
  /** Initialize RTC Only
  */
  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 255;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutRemap = RTC_OUTPUT_REMAP_NONE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  hrtc.Init.OutPutPullUp = RTC_OUTPUT_PULLUP_NONE;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN RTC_Init 2 */

  /* USER CODE END RTC_Init 2 */

}

/**
  * @brief TIM16 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM16_Init(void)
{

  /* USER CODE BEGIN TIM16_Init 0 */

  /* USER CODE END TIM16_Init 0 */

  /* USER CODE BEGIN TIM16_Init 1 */

  /* USER CODE END TIM16_Init 1 */
  htim16.Instance = TIM16;
  htim16.Init.Prescaler = 6400-1;
  htim16.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim16.Init.Period = 10000-1;
  htim16.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim16.Init.RepetitionCounter = 0;
  htim16.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim16) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM16_Init 2 */

  /* USER CODE END TIM16_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.Init.ClockPrescaler = UART_PRESCALER_DIV1;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, c_Pin|g_Pin|d_Pin|f_Pin
                          |a_Pin|b_Pin|e_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, s0_Pin|s1_Pin|s2_Pin|s3_Pin
                          |h_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LD3_GPIO_Port, LD3_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : c_Pin g_Pin d_Pin f_Pin
                           a_Pin b_Pin e_Pin */
  GPIO_InitStruct.Pin = c_Pin|g_Pin|d_Pin|f_Pin
                          |a_Pin|b_Pin|e_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : T_NRST_Pin */
  GPIO_InitStruct.Pin = T_NRST_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(T_NRST_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : s0_Pin s1_Pin s2_Pin s3_Pin
                           h_Pin */
  GPIO_InitStruct.Pin = s0_Pin|s1_Pin|s2_Pin|s3_Pin
                          |h_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : LD3_Pin */
  GPIO_InitStruct.Pin = LD3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LD3_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : btn_Pin */
  GPIO_InitStruct.Pin = btn_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(btn_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI2_3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI2_3_IRQn);

}

/* USER CODE BEGIN 4 */
void test_led(void)
{
	a_toggle;
	b_toggle;
	c_toggle;
	d_toggle;
	e_toggle;
	f_toggle;
	g_toggle;
	h_toggle;
	
	s0_on;
	s1_on;
	s2_on;
	s3_on;
}

int convert_to(int x)
{
	int temp;
	switch (x)
	{
		case 0:			
      temp = n_0;
      break;
    case 1:
      temp = n_1;
      break;
    case 2:
      temp = n_2;
      break;
    case 3:
      temp = n_3;
      break;
    case 4:
      temp = n_4;
      break;
    case 5:
      temp = n_5;
      break;
    case 6:
      temp = n_6;
      break;
    case 7:
      temp = n_7;
      break;
    case 8:
      temp = n_8;
      break;
    case 9:
      temp = n_9;
      break;
		default:
			temp = 0x00;
			break;
   }
	return temp;
}

void print_to(int x)
{
	for(int i = 0; i < 7; i++)
	{
		HAL_GPIO_WritePin(a_GPIO_Port, segment[i], (1 & x));
		x >>= 1;
	}
}

void selector(int x)
{
	switch (x)
	{
		case 0:
			s3_off;
			s2_off;
			s1_off;
			s0_on;
			break;
		case 1:
			s3_off;
			s2_off;
			s1_on;
			s0_off;
			break;
		case 2:
			s3_off;
			s2_on;
			s1_off;
			s0_off;
			break;
		case 3:
			s3_on;
			s2_off;
			s1_off;
			s0_off;
			break;
	}
}

void set_time (void)
{	
	gtime.Hours = 0x08;
	gtime.Minutes = 0x1D;
	gtime.Seconds = 0x0; 	
	gtime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE; 
  gtime.StoreOperation = RTC_STOREOPERATION_RESET; 	
	HAL_RTC_SetTime(&hrtc, &gtime, RTC_FORMAT_BIN);

	gdate.WeekDay = RTC_WEEKDAY_FRIDAY;
	gdate.Month = RTC_MONTH_DECEMBER;	
	gdate.Date = 0x10;
	gdate.Year = 0x16;	
	HAL_RTC_SetDate(&hrtc, &gdate, RTC_FORMAT_BIN);
	
	HAL_RTCEx_BKUPWrite(&hrtc, RTC_BKP_DR1, 0x32F2); // backup register
}

void get_time(void)
{
	HAL_RTC_GetTime(&hrtc, &gtime, RTC_FORMAT_BIN);
	jam = gtime.Hours;
	menit = gtime.Minutes;
	detik = gtime.Seconds;
	mildetik = gtime.SubSeconds;
	
	HAL_RTC_GetDate(&hrtc, &gdate, RTC_FORMAT_BIN);
	
	//UNUSED(gdate);
	
	uart_buf_len = sprintf(uart_buf,"%d, %02d-%02d-%2d | %02d;%02d;%02d\r\n",gdate.WeekDay, gdate.Date, gdate.Month, 2000 + gdate.Year, gtime.Hours, gtime.Minutes, gtime.Seconds); 
	HAL_UART_Transmit(&huart2, (uint8_t *)uart_buf, uart_buf_len, 100);
}

void display_sevensegment(short a, short b, short c, short d)
{		
	print_to(convert_to(s0));		
	selector(0);	
	HAL_Delay(2);			
	if(s1 != 0 || s1 == 0 && s2 != 0 || s3 != 0)
	{
		print_to(convert_to(s1));	
		selector(1);		
		HAL_Delay(2);		
		if(s2 != 0 || s2 == 0 && s3 != 0)
		{
			s2_status = 1;
			print_to(convert_to(s2));			
			selector(2);			
			HAL_Delay(2);			
			if(s3 != 0 )
			{
				print_to(convert_to(s3));
				selector(3);				
				HAL_Delay(2);	
			}
		}
	}
		
	if (s2_status == 1 && s2 == 0 && s3 == 0)
	{
		print_to(convert_to(666));
		selector(2);
		HAL_Delay(2);	
	}
}

void watch_mode(void)
{
	get_time();
	if ( s != detik)
	{
		s = detik;
		h_toggle;
		s2_status = HAL_GPIO_ReadPin(h_GPIO_Port, h_Pin);
	}
	
	s3 = jam % 100 / 10;
	s2 = jam % 10;
	s1 = menit % 100 / 10;
	s0 = menit % 10;
	
	display_sevensegment(s3, s2, s1, s0);	
}

void counter_mode(void)
{
	get_time();
	
	if(stop_watch == 1)
	{
		if(time_now != mildetik)
		{
			time_now = mildetik;				
			count_numb++;					
			if(count_numb > 100)
			{
				count_numb = 0;				
				count_second++;	
				if(count_second > 59)
				{
					count_second = 0;
					count_minute++;
					if(count_minute > 59)
					{
						count_minute = 0;
						count_hour++;
						if(count_hour > 23)
						{
							count_hour = 0;
						}
					}
				}								
		  }
		}
		if ( s != count_second)
		{
			s = count_second;
			h_toggle;
			s2_status = HAL_GPIO_ReadPin(h_GPIO_Port, h_Pin);
		}
	}
	
	else
	{
		time_now = HAL_GetTick();
		time_last = time_last + time_now;
		
		if ( s != detik)
		{
			s = detik;
			h_toggle;
			s2_status = HAL_GPIO_ReadPin(h_GPIO_Port, h_Pin);
		}
	}

	s3 = count_second % 100 / 10;
	s2 = count_second % 10;
	s1 = count_numb % 100 / 10;
	s0 = count_numb % 10;
	
	if(count_minute != 0)
	{
		s3 = count_minute % 100 / 10;
		s2 = count_minute % 10;
		s1 = count_second % 100 / 10;
		s0 = count_second % 10;
	}
	
	else if(count_hour != 0)
	{
		s3 = count_hour % 100 / 10;
		s2 = count_hour % 10;
		s1 = count_minute % 100 / 10;
		s0 = count_minute % 10;
	}
		
	display_sevensegment(s3, s2, s1, s0);
}

void switch_mode (int sw)
{
	switch(sw)
	{
		case 0:
			watch_mode();
			break;
		case 1:
			counter_mode();
			break;			
	}
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
